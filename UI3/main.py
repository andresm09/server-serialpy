from kivy.app import App
from kivy.uix.screenmanager import Screen, ScreenManager 
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.properties import ObjectProperty


class ScreenLogin(Screen):
    def __init__(self, **kwargs):
        global passTxt
        global userTxt
        # make sure we aren't overriding any important functionality
        super(ScreenLogin, self).__init__(**kwargs)
        myFloatLayout = FloatLayout()
        dayInput = TextInput(size_hint= (.05, .05),hint_text= "dia",
                              pos_hint= {'center_x': .3, 'center_y': .2},
                              multiline=False)
        monthInput = TextInput(size_hint= (.05, .05),hint_text= "mes",
                              pos_hint= {'center_x': .4, 'center_y': .2},
                              multiline=False)
        #passInput = TextInput(hint_text= "Password",password = True,
        #                      size_hint= (.5, .1),pos_hint= {'center_x': .5, 
        #                      'center_y': .5}, multiline=False)
 
        im = Image(source = 'index.png',
                              size_hint= (1, 1),pos_hint= {'center_x': .7, 
                              'center_y': .6})
        im2 = Image(source = 'index.png',
                              size_hint= (1, 1),pos_hint= {'center_x': .3, 
                              'center_y': .6})
        btnFind = Button(text= "Buscar",size_hint= (.2, .1),
                          pos_hint= {'center_x': .6, 'center_y': .2})
        
        
        myFloatLayout.add_widget(im)
        myFloatLayout.add_widget(im2)
        myFloatLayout.add_widget(dayInput)
        myFloatLayout.add_widget(monthInput)
        myFloatLayout.add_widget(btnFind)
        self.add_widget(myFloatLayout)
    


class TestApp(App):
        def build(self):
            my_screenmanager = ScreenManager()
            screen1 = ScreenLogin(name='screen1')
            my_screenmanager.add_widget(screen1)
            return my_screenmanager

if __name__ == '__main__':
    TestApp().run()

