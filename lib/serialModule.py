import serial
import time

#interpolacion lineal
def interp(valu):
    return (5.0 * float(valu) * 100.0)/1024.0


def getSerial():
    try:
        ser = serial.Serial('/dev/cu.usbmodem1411', 9600, timeout=0) # establece la comunicacion con el serial
        print "Conectado"
    except IOError, e:
        print "Error: No hay dispositivos "
        sys.exit()

    vl = ser.readline()
    try:
        y = interp(float(vl))
        return y
    except ValueError,e:
        y = 20.0
        return y
