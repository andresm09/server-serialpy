﻿
--------TABLA USUARIOS -----
CREATE TABLE usuario(
idUsuario INT PRIMARY KEY AUTO_INCREMENT,
username VARCHAR(20) NOT NULL,
pass VARCHAR(35) NOT NULL,
fechaRegistro DATE NOT NULL,
);
correo VARCHAR(40)


----------TABLA TEMPERATURAS ----------
CREATE TABLE temperature(
idTemperatura INT PRIMARY KEY AUTO_INCREMENT,
fechaConsulta TIMESTAMP NOT NULL,
tipo varchar(20) NOT NULL,
temperaturaC DECIMAL(10,2),
sensorX FLOAT(10,2),
sensorY FLOAT(10,2),
sensorZ FLOAT(10,2),
idUsuario INT
);

--------Relacion----------------------
ALTER TABLE temperature
ADD CONSTRAINT FOREIGN KEY (idUsuario)
REFERENCES usuario(idUsuario)
ON DELETE CASCADE
ON UPDATE CASCADE;

--------INSERTS---------------

INSERT INTO usuario VALUES('1','Juan','juan123','2017/04/06');
INSERT INTO usuario VALUES('2','Pedro','pedro'123','2017/04/06');
INSERT INTO usuario VALUES('3','Jorge','jorge123','2017/04/06');
INSERT INTO usuario VALUES('4','Andres','andres123','2017/04/06');n
INSERT INTO usuario VALUES('5','Alexis','alexis123','2017/04/06');
INSERT INTO usuario VALUES('6','Jobani','jobani123','2017/04/06');

INSERT INTO temperature VALUES('1','2017-04-06 12:56:00','Serial','25','34.2','23.5','21.2','2');
INSERT INTO temperature VALUES('2','2017-04-06 01:57:00','Serial','25','34.2','23.5','21.2','1');
INSERT INTO temperature VALUES('3','2017-04-06 01:58:00','Serial','25','34.2','23.5','21.2','3');
INSERT INTO temperature VALUES('4','2017-04-06 01:59:00','Serial','25','34.2','23.5','21.2','4');
INSERT INTO temperature VALUES('5','2017-04-06 02:00:00','Serial','25','34.2','23.5','21.2','5');
INSERT INTO temperature VALUES('6','2017-04-06 02:10:00','Serial','25','34.2','23.5','21.2','6');
INSERT INTO temperature VALUES('7','2017-04-06 01:58:00','Serial','25','34.2','23.5','21.2','3');
INSERT INTO temperature VALUES('8','2017-04-06 01:58:00','Tcp','25','34.2','23.5','21.2','3');

$verTweets = "SELECT temperature FROM interfaces_ll WHERE idUsuario=(SELECT idUsuario FROM usuario 
				   WHERE username = '') ORDER BY fechaTweet;";
	$resultadoTweets=$conexion->query($verTweets);

Select * From temperature
Where idUsuario=(Select idUsuario From usuario
Where username='Jorge' AND tipo='serial')







