# README #

### What is this repository for? ###

* Updating MySQL Server with interface values from MCU [Serial, TCP/IP, RJ-45]
* 1.0.1

### How do I get set up? ###

* Install MySQL server
* Install Kivy
* Install mySQLdb-python package through Kivy (Python 2.7)
* Install serial package through Kivy (Python 2.7)
* Run a local MySQL database after running the sql script founded in the sqlServer folder

### Contribution guidelines ###

* Use testing branch before push to master

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
