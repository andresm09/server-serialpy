from kivy.app import App
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.properties import ObjectProperty
from kivy.uix.textinput import TextInput
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from lib.checkModule import getValidation

class ScreenLogin(Screen):
    def __init__(self, **kwargs):
        global passTxt
        global userTxt
        # make sure we aren't overriding any important functionality
        super(ScreenLogin, self).__init__(**kwargs)
        myFloatLayout = FloatLayout()
        userInput = TextInput(size_hint= (.5, .1),hint_text= "Username",
                              pos_hint= {'center_x': .5, 'center_y': .7},
                              multiline=False)
        def UserInputText(instance, value):
            global userTxt
            userTxt = (value)
        userInput.bind(text = UserInputText)
        passInput = TextInput(hint_text= "Password",password = True,
                              size_hint= (.5, .1),pos_hint= {'center_x': .5,
                              'center_y': .5}, multiline=False)
        def PassInputText(instance, value):
            global passTxt
            passTxt = (value)
        passInput.bind(text = PassInputText)
        btnLogin = Button(text= "Login",size_hint= (.2, .1),
                          pos_hint= {'center_x': .5, 'center_y': .3})
        btnLogin.bind(on_press=self.changer)
        myFloatLayout.add_widget(userInput)
        myFloatLayout.add_widget(passInput)
        myFloatLayout.add_widget(btnLogin)
        self.add_widget(myFloatLayout)

    def changer(self,*args):
        if getValidation(userTxt,passTxt) == 1:
            self.manager.current = 'screen2'


class ScreenMonitor(Screen):
    def __init__(self, **kwargs):
        # make sure we aren't overriding any important functionality
        super(ScreenMonitor, self).__init__(**kwargs)
        # let's add a Widget to this layout
        my_box1 = GridLayout(cols=3, spacing=40, size_hint_y=5)
        my_box1.bind(minimum_height=my_box1.setter('height'))
        root = ScrollView(size_hint=(1, None), size=(Window.width, Window.height))
        tipeLabel = Label(text= "Tipo",color = (1, 1, 1, 1))
        tempLabel = Label(text= "Temperatura",color = (1, 1, 1, 1))
        senalLabel = Label(text= "Temperatura",color = (1, 1, 1, 1))
        my_box1.add_widget(tipeLabel)
        my_box1.add_widget(tempLabel)
        my_box1.add_widget(senalLabel)
        for i in range(90):
            tipeValue = Label(text= str(i),color = (1, 1, 1, 1))
            my_box1.add_widget(tipeValue)
        root.add_widget(my_box1)
        self.add_widget(root)

    def changer(self,*args):
        self.manager.current = 'screen1'

class TestApp(App):
        def build(self):
            my_screenmanager = ScreenManager()
            screen1 = ScreenLogin(name='screen1')
            screen2 = ScreenMonitor(name='screen2')
            my_screenmanager.add_widget(screen1)
            my_screenmanager.add_widget(screen2)
            return my_screenmanager

if __name__ == '__main__':
    TestApp().run()
